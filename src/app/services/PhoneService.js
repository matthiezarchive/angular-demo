import RestBase from '../lib/RestBase'

export default class PhoneService extends RestBase {
  /*@ngInject*/
  constructor(Restangular) {
    super(Restangular)
    this.base = "phones"
  }
}