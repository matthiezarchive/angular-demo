import RestBase from '../lib/RestBase'

export default class UserService extends RestBase {
  /*@ngInject*/
  constructor(Restangular) {
    super(Restangular)
    this.base = 'users'
  }
}