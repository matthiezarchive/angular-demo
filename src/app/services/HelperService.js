export default class HelperService {
  /*@ngInject*/
  constructor($window) {
    this.$window = $window
  }

  googleLogout() {
    this.$window.gapi.auth.setToken(null);
    this.$window.gapi.auth.signOut()
  }
}