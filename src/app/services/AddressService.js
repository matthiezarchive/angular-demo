export default class AddressService {
  static build(addresses) {
    const deliveryData = AddressService.buildAddresses(addresses, 'delivery')
    const billingData = AddressService.buildAddresses(addresses, 'billing')

    return {
      deliveryAddresses: {
        selected: AddressService.getSelected(deliveryData),
        data: deliveryData
      },
      billingAddresses: {
        selected: AddressService.getSelected(billingData),
        data: billingData
      }
    }
  }

  static buildAddresses(addresses, type) {
    return addresses.filter(a => a.type === type).map(d => {
      d.label = `${d.name}, ${d.street}, ${d.zip} ${d.city}`
      return d
    })
  }

  static getSelected(addresses) {
    if (!addresses.length) return null
    const primaryAddress = addresses.findIndex(a => a.primary === true)
    return primaryAddress === -1 ? addresses[0] : addresses[primaryAddress]
  }
}