export default [
  {
    name: 'home',
    url: '/',
    template: '<landing-page></landing-page>',
  },
  {
    name: 'auth',
  },
  {
    name: 'auth.user',
    url: '/user',
    template: '<user-detail></user-detail>',
  },
  {
    name: 'phones',
    url: '/phones',
    template: '<phone-list></phone-list>',
  },
  {
    name: 'phone',
    url: '/phones/:phoneId',
    template: '<phone-detail></phone-detail>',
  },
  {
    name: 'steamNews',
    url: '/steam-news',
    template: '<steam-news></steam-news>',
  },
  {
    name: 'steamGameNews',
    url: '/steam-news/:appId',
    template: '<steam-game-news></steam-game-news>',
  },
]