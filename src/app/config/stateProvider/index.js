import states from "./states"

/*@ngInject*/
export default $stateProvider => states.forEach(r => $stateProvider.state(r))