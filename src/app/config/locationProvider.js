/*@ngInject*/
export default $locationProvider => {
  $locationProvider.html5Mode(true)
  $locationProvider.hashPrefix("")
}