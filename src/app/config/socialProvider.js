/*@ngInject*/
export default (constants, socialProvider) => socialProvider.setGoogleKey(constants.googleClientId);