/*@ngInject*/
export default ($localStorageProvider, constants) => $localStorageProvider.setKeyPrefix(`${constants.abbreviation}_`);