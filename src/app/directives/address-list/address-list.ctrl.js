export default class AddressListCtrl {
  /*@ngInject*/
  constructor($scope, {user}) {
    this.addresses = $scope.type.toLowerCase() === 'billing' ? user.billingAddresses : user.deliveryAddresses
  }
}