import angular from 'angular'
 import AddressListCtrl from './address-list.ctrl'

const NAME = 'addressList'

angular
  .module(NAME, [])
  .directive(NAME, ['$rootScope', () => {
    return {
      restrict: 'E',

      controller: ['$scope', '$rootScope', AddressListCtrl],

      controllerAs: 'vm',

      templateUrl: 'app/directives/address-list/address-list.tpl.html',

      scope: {
        type: '=',
        onChangeAddress: '&'
      },
    }
  }])

export default NAME