export default class QuickbuyCtrl {
  constructor($scope, $rootScope) {
    const {user} = $rootScope
    $scope.name = 'quickbuyModal'
    $scope.user = user

    $scope.toggleQuickbuyModal = () => {
      $scope.showModal = !$scope.showModal
    }
  }
}