import angular from 'angular'
import QuickbuyCtrl from './quickbuy.ctrl'
import addressList from '../address-list/address-list.directive'

const NAME = 'quickbuy'

angular
  .module(NAME, [addressList])
  .directive(NAME, () => {
  return {
    templateUrl: 'app/directives/quickbuy/quickbuy.tpl.html',
    controller: ['$scope', '$rootScope', QuickbuyCtrl],

    link(scope) {
      scope.$watch('showModal', bool => {
        if (bool) {
          scope.displayStyle = 'block'
          scope.modalFadeIn = 'in'
        }
        else {
          scope.displayStyle = 'none'
          scope.modalFadeIn = ''
        }
      })
    }
  }
})

export default NAME