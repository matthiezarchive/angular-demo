import angular from 'angular'
import AlertsCtrl from './alerts.ctrl'
import AlertsService from './alerts.service'

const NAME = 'alerts'

angular
  .module(NAME, [])
  .service('AlertsService', AlertsService)
  .directive(NAME, () => {
    return {
      restrict: 'E',
      templateUrl: 'app/directives/alerts/alerts.tpl.html',
      controller: AlertsCtrl,
      controllerAs: 'vm',

      scope: {
        onChangeAddress: '&'
      },

      link(scope, jql, atts, ctrl) {},
    }
  })

export default NAME