export default class AlertsService {
  /*@ngInject*/
  constructor($rootScope) {
    this.$rootScope = $rootScope

    $rootScope.alerts = [];
  }

  add(msg) {
    this.$rootScope.alerts.push(msg)
  }

  close(index) {
    this.$rootScope.alerts.splice(index, 1)
  }
}