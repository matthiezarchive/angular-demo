export default class NavigationController {
  /*@ngInject*/
  constructor($rootScope, socialLoginService) {
    this.user = $rootScope.user
    this.socialLoginService = socialLoginService

    this.navbar = {
      id: "header",
      links: [
        {sref: "home", title: "Home"},
        {sref: "phones", title: "Phones"},
        {sref: "steamNews", title: "Steam News"}
      ],
    }
  }

  logout() {
    this.socialLoginService.logout()
  }
}