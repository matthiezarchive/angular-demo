import angular from "angular"
import NavigationDirective from "./navigation.directive"

const NAME = "navigation"

angular
  .module(NAME, [])
  .directive(NAME, NavigationDirective)
export default NAME