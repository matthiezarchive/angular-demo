import angular from 'angular'
import SteamGameNewsCtrl from './steam-game-news.ctrl'
import {checkmark} from '../../lib/filters'

const NAME = 'steamGameNews'

 angular
  .module(NAME, [])
  .component(NAME, {
    templateUrl: 'app/components/steam-game-news/steam-game-news.tpl.html',
    controller: SteamGameNewsCtrl,
    controllerAs: 'vm'
  })
   .filter('checkmark', checkmark)

export default NAME