export default class SteamGameNewsCtrl {
  /*@ngInject*/
  constructor($stateParams, $window, $rootScope) {
    this.orderProp = 'id'

    this.news = []

   const API_BASE = '//api.steampowered.com';
    const appid = $stateParams && $stateParams.appId || 730
    const format = 'json'
    const path = `${API_BASE}/ISteamNews/GetNewsForApp/v0002/?appid=${appid}`

    $window.fetch(path, {})
      .then(res => res.json())
      .then(json => {
        const newsItems = json.appnews.newsitems
        if (Array.isArray(newsItems)) {
          console.log({newsItems})
          this.news = newsItems
          $rootScope.$apply()
        }
        else console.warn('API-banned?', {newsItems})
      })
      .catch(err => { throw err })
  }

  transformDate(date) {
    return new Date(date).toLocaleString()
  }
}