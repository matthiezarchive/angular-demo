export default class SteamNewsCtrl {
  /*@ngInject*/
  constructor($state) {
    this.$state = $state

    this.appId = null
  }

  onChangeAppId() {
    this.$state.go('steamGameNews', { appId: this.appId }, {reload: true})
  }
}