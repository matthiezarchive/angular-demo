import angular from 'angular'
import SteamNewsCtrl from './steam-news.ctrl'
import {checkmark} from '../../lib/filters'

const NAME = 'steamNews'

 angular
  .module(NAME, [])
  .component(NAME, {
    templateUrl: 'app/components/steam-news/steam-news.tpl.html',
    controller: SteamNewsCtrl,
    controllerAs: 'vm'
  })
   .filter('checkmark', checkmark)

export default NAME