import AddressService from '../../services/AddressService'
import {addressExcludeKeys} from './constants'

export default class UserDetailCtrl {
  /*@ngInject*/
  constructor($rootScope, UserService) {
    // console.log({$rootScope})
    this.user = $rootScope.user

    // console.log({self: this})

    this.onChangeAddress = address => {
      UserService.one(this.user.id)
        .then(oldUser => {
          const allAddresses = oldUser.addresses
          const addresses = allAddresses.filter(a => a.type === address.type)
          const index = addresses.findIndex(a => a.id === address.id)

          if (index !== -1) {
            addresses.forEach((a, i) => a.primary = i === index)
            oldUser.addresses = addresses
            oldUser
              .save()
              .then(newUser => console.log({newUser}))
              .catch(err => console.error({err}))
          }
        })
    }
  }
}