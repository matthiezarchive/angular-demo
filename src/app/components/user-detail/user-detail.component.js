import angular from 'angular'
import UserDetailCtrl from './user-detail.ctrl'

const NAME = 'userDetail'

angular
  .module(NAME, [])
  .component(NAME, {
    templateUrl: 'app/components/user-detail/user-detail.tpl.html',
    controller: UserDetailCtrl,
    controllerAs: 'vm',
  })

export default NAME