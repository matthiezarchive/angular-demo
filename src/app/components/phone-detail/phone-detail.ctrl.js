export default class PhoneDetailCtrl {
  /*@ngInject*/
  constructor($stateParams, PhoneService) {
    PhoneService.one($stateParams.phoneId).then(phone => {
      this.phone = phone

      this.mainImageUrl = this.phone.images[0]
    })
  }
}