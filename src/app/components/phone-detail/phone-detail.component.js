import angular from "angular"
import PhoneDetailCtrl from "./phone-detail.ctrl"

const NAME = "phoneDetail"

angular
  .module(NAME, ["ngAnimate"])
  .animation(".phone", () => {
    return {
      addClass(element, className, done) {
        if (className !== "selected") return
        element = $(element)
        element.css({
          display: "block",
          position: "absolute",
          top: 500,
          left: 0
        })
        $(element).animate({
          top: 0
        }, done)

        return function (wasCanceled) {
          if (wasCanceled) element.stop()
        }
      },

      removeClass(element, className, done) {
        if (className !== "selected") return
        element = $(element)
        element.css({
          position: "absolute",
          top: 0,
          left: 0
        })
        element.animate({
          top: -500
        }, done)

        return function (wasCanceled) {
          if (wasCanceled) element.stop()
        }
      }
    }
  })
  .component(NAME, {
    templateUrl: "app/components/phone-detail/phone-detail.tpl.html",
    controller: PhoneDetailCtrl,
    controllerAs: "vm",
  })
export default NAME