import angular from "angular"
import LandingPageController from "./landing-page.controller"

const NAME = "landingPage"

angular.module(NAME, [])
  .component(NAME, {
    templateUrl: "app/components/landing-page/landing-page.tpl.html",
    controller: LandingPageController,
    controllerAs: "vm"
  })

export default NAME