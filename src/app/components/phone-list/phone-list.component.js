import angular from 'angular'
import PhoneListCtrl from './phone-list.ctrl'
import quickbuy from '../../directives/quickbuy/quickbuy.directive'
import {checkmark} from '../../lib/filters'
const NAME = 'phoneList'

 angular
  .module(NAME, [quickbuy])
  .component(NAME, {
    templateUrl: 'app/components/phone-list/phone-list.tpl.html',
    controller: PhoneListCtrl,
    controllerAs: 'vm'
  })
   .filter('checkmark', checkmark)

export default NAME