export default class PhoneListCtrl {
  /*@ngInject*/
  constructor(PhoneService) {
    PhoneService.all().then(phones => this.phones = phones)
    this.orderProp = 'age'
  }
}