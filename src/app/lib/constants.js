const constants = {}

Object.defineProperty(constants, 'googleClientId', {
  value: '952433149756-80t6s0u125tho31413f10a106s76ndch.apps.googleusercontent.com',
  writable: false
})

Object.defineProperty(constants, 'abbreviation', {
  value: "app",
  writable: false
})

export default constants