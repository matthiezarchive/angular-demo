export const checkmark = () => bool => bool ? '\u2713' : '\u2718'

export default {
  checkmark
}