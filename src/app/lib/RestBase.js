export default class RestBase {
  /*@ngInject*/
  constructor(Restangular) {
    Restangular.setRequestSuffix('.json')
    Restangular.setBaseUrl('assets/data/')
    Restangular.addRequestInterceptor((ele, op, what, url) => {})

    this.Restangular = Restangular
    this.base = ''
  }

  one(id) {
    return this.Restangular.one(this.base, id)
      .get()
      .then(row => row)
  }

  all() {
    return this.Restangular.all(this.base)
      .getList()
      .then(list => list)
  }
}