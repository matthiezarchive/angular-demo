// const request = require('request-promise');
// const SteamID = require('steamid');
import axios from 'axios'
import SteamID from 'steamid'

const buildSteamids = steamids => {
  const MAX_STEAMIDS = 100;

  if (!Array.isArray(steamids)) steamids = [steamids];

  if (steamids.length > MAX_STEAMIDS) throw new Error(`Cannot request more than ${MAX_STEAMIDS} steamids at once.`);

  return steamids.map(s => new SteamID(s).getSteamID64()).join(',');
};

export const ERROR_CODES = {
  400: 'Bad Request. Please verify that all required parameters are being sent.',
  401: 'Unauthorized. Access is denied. Retrying will not help. Please verify your key= parameter.',
  403: 'Forbidden. Access is denied. Retrying will not help. Please verify your key= parameter.',
  404: 'Not Found. The API requested does not exists.',
  405: 'Method Not Allowed. This API has been called with a the wrong HTTP method like GET or PUSH.',
  429: 'Too Many Requests. You are being rate limited.',
  500: 'Internal Server Error. An unrecoverable error has occurred, please try again. If this continues to persist then please post to the Steamworks developer discussion with additional details of your request.',
  503: 'Service Unavailable. Server is temporarily unavailable, or too busy to respond. Please wait and try again later.'
};

const API_BASE = 'https://api.steampowered.com';
const ISteamUser = `${API_BASE}/ISteamUser/`
const ISteamUserStats = `${API_BASE}/ISteamUserStats/`
const v1 = '/v1/'
const v2 = '/v0002/'

export default class SteamApi {
  constructor(apiKey, appid = 730, format = 'json') {
    this.appid = appid;
    this.apiKey = apiKey;
    this.format = format;
  }

  _requestHandler(data) {
    const KEY = 'response';
    if (KEY in data) data = data[KEY];

    return this.format === 'json' ? JSON.parse(data) : data;
  }

  async getPlayerBans(steamids, format = this.format) {
    return this._requestHandler(await request.get(`${ISteamUser}GetPlayerBans${v1}?key=${this.apiKey}&steamids=${buildSteamids(steamids)}&format=${format}`));
  }

  async getPlayerSummaries(steamids, format = this.format) {
    return this._requestHandler(await request.get(`${ISteamUser}GetPlayerSummaries${v2}?key=${this.apiKey}&steamids=${buildSteamids(steamids)}&format=${format}`));
  }

  async getFriendList(steamid, relationship = 'friend', format = this.format) {
    return this._requestHandler(await request.get(`${ISteamUser}GetFriendList/v0001/?key=${this.apiKey}&steamid=${steamid}&relationship=${relationship}&format=${format}`));
  }

  async getGameStats(steamid, appid = this.appid, format = this.format) {
    return this._requestHandler(await request.get(`${ISteamUserStats}GetUserStatsForGame${v2}?appid=${appid}&key=${this.apiKey}&steamid=${steamid}&format=${format}`));
  }

  async getNumberOfCurrentPlayers(appid = this.appid, format = this.format) {
    return this._requestHandler(await request.get(`${ISteamUserStats}GetNumberOfCurrentPlayers${v1}?appid=${appid}&format=${format}`));
  }

  async getNewsForApp(appid = this.appid, count = 3, maxlength = 300, format = this.format) {
    return this._requestHandler(await request.get(`${API_BASE}/ISteamNews/GetNewsForApp${v2}?appid=${appid}&count=${count}&maxlength=${maxlength}&format=${format}`));
  }

  async getSchemaForGame(appid = this.appid, format = this.format) {
    return this._requestHandler(await request.get(`${ISteamUserStats}GetSchemaForGame/v2/?key=${this.apiKey}&appid=${appid}&format=${format}`));
  }
}