import ngAnimate from "../../node_modules/angular-animate/index"
import ngRoute from "../../node_modules/angular-route/index"
import ngBootstrap from "angular-ui-bootstrap"
import ngUiRouter from "@uirouter/angularjs"
import ngRestangular from "restangular"
import ngStorage from "ngstorage"
import 'angularjs-social-login'

import landingPage from "./components/landing-page/landing-page.component"
import phoneList from "./components/phone-list/phone-list.component"
import phoneDetail from "./components/phone-detail/phone-detail.component"
import userDetail from "./components/user-detail/user-detail.component"
import steamGameNews from "./components/steam-game-news/steam-game-news.component"
import steamNews from "./components/steam-news/steam-news.component"
import alerts from "./directives/alerts/alerts.directive"
import navigation from "./directives/navigation/navigation.module"

export default [
  ngAnimate, ngRoute, ngBootstrap, ngRestangular, ngStorage.name,
  'socialLogin', ngUiRouter, landingPage, phoneList, phoneDetail, userDetail,
  alerts, navigation, steamGameNews, steamNews]