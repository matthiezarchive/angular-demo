/*@ngInject*/
export default ($rootScope, AlertsService, $transitions, $trace) => {
  // $trace.enable("TRANSITION")

  const transitions = [
    {
      to: "auth.**",
      fn: transition => {
        if ($rootScope.user.authenticated === false) {

          AlertsService.add({
            msg: `You need to be authenticated to access this part of the site!`,
            type: "danger",
            timeout: 6000
          })

          return transition.router.stateService.target("home")
        }
      }
    }
  ]

  transitions.forEach(t => $transitions.onStart({to: t.to}, t.fn))
}