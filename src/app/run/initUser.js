/*@ngInject*/
export default ($rootScope, $localStorage) => {
  $rootScope.user = $localStorage.user || {
    authenticated: false,
    google: {}
  }
}