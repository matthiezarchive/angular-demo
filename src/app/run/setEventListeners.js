/*@ngInject*/
export default ($rootScope, AlertsService, $localStorage) => {
  const isGoogleProvider = user => user.provider === "google"

  const events = [
    {
      name: "event:social-sign-in-success",
      fn: onSocialSignInSuccess,
    },
    {
      name: "event:social-sign-out-success",
      fn: onSogialSignOut,
    },
    {
      name: "$locationChangeStart",
      fn: onLocationChangeStart,
    },
    {
      name: "$locationChangeSuccess",
      fn: onLocationChangeSuccess,
    },
    {
      name: "$stateChangeStart",
      fn: onStateChangeStart,
    },
  ]

  function onSocialSignInSuccess(ev, user) {
    if (isGoogleProvider(user)) {
      $rootScope.user.authenticated = true
      $rootScope.user.google = user

      $localStorage.user = $rootScope.user

      AlertsService.add({
        msg: `Hi ${$rootScope.user.google.name}, you have been authenticated via Google!`,
        type: "success",
        timeout: 3000
      })

      $rootScope.$apply()
    }
  }

  function onSogialSignOut(ev, user) {
    console.log("onSogialSignOut", {ev, user})
    $localStorage.user.authenticated = false

    if (isGoogleProvider(user)) $localStorage.user.google = {}
  }

  function onLocationChangeStart(ngEvent, newUrl, oldUrl, newState, oldState) {
  }

  function onLocationChangeSuccess(ngEvent, newUrl, oldUrl, newState, oldState) {
  }

  function onStateChangeStart(ngEvent, newUrl, oldUrl, newState, oldState) {
    console.log("onStateChangeStart", {
      ngEvent,
      newUrl,
      oldUrl,
      newState,
      oldState
    })
  }

  events.forEach(e => $rootScope.$on(e.name, e.fn))
}