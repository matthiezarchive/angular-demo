import "../../node_modules/bootstrap/dist/css/bootstrap.min.css"
import "../assets/css/app.css"
import "../assets/css/app.animations.css"
import jQuery from "jquery"
window.$ = window.jQuery = jQuery

import angular from "angular"

import dependencies from "./dependencies"
import constants from './lib/constants'
import PhoneService from "./services/PhoneService"
import UserService from "./services/UserService"
import HelperService from "./services/HelperService"
import configLocationProvider from "./config/locationProvider"
import configStateProvider from "./config/stateProvider"
import configLocalStorageProvider from "./config/localStorageProvider"
import configSocialProvider from "./config/socialProvider"
import initUser from "./run/initUser"
import setEventListeners from "./run/setEventListeners"
import setTransitionListeners from "./run/setTransitionListeners"

angular
  .module("app", dependencies)
  .constant('constants', constants)
  .service("PhoneService", PhoneService)
  .service("UserService", UserService)
  .service("HelperService", HelperService)
  .config(configLocationProvider)
  .config(configStateProvider)
  .config(configLocalStorageProvider)
  .config(configSocialProvider)
  .run(/*@ngInject*/ $animate => $animate.enabled(true))
  .run(initUser)
  .run(setEventListeners)
  .run(setTransitionListeners)
