const path = require("path")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const CleanWebpackPlugin = require("clean-webpack-plugin")
const webpack = require("webpack")

const BASE = 'src'
const DEV_SERVER_PORT = 8080
const BASE_PATH = `./${BASE}/`
const DIST_BASE = `${BASE}/assets/dist/js`

module.exports = {
  mode: "development",

  devServer: {
    contentBase: path.join(__dirname, BASE),
    compress: true,
    index: "index.html",
    inline: true,
    lazy: false,
    overlay: true,
    historyApiFallback: true,
    hot: true,
    host: "localhost",
    port: DEV_SERVER_PORT,
    headers: {'Access-Control-Allow-Origin': '*'},
  },

  devtool: "cheap-module-eval-source-map", //eval

  entry: {
    app: BASE_PATH + "app/index.js",
  },

  output: {
    filename: "[name].bundle.js",
    path: path.resolve(__dirname, DIST_BASE),
  },

  plugins: [
    new CleanWebpackPlugin([DIST_BASE]),
    new webpack.NamedModulesPlugin(),
    new HtmlWebpackPlugin({template: `${BASE}/index.html`}),
    new webpack.HotModuleReplacementPlugin(), // dont load twice by specifying "--hot" start parameter, loading twice causes error
    new webpack.NoEmitOnErrorsPlugin()
  ],

  module: {
    rules: [
      {
        test: /src.*\.js$/,
        loader: "ng-hot-reload-loader!babel-loader"
      },
      {
        test: /src.*\.js$/, loader: "babel-loader", options: {
          "presets": ["env",], "plugins": [["angularjs-annotate"],],
        },
      },
      {
        test: /\.html$/, loader: "html-loader",
      },
      {
        test: /\.css$/, use: ["style-loader", "css-loader",],
      },
      {
        test: /\.(png|svg|jpg|gif)$/, loader: "file-loader",
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ["file-loader", "url-loader",],
      },
    ],
  },

  resolve: {
    modules: ["node_modules", path.resolve(__dirname, `${BASE}/assets/js`)]
  }
}